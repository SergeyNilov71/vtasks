import appMixin from '@/appMixin'
import axios from 'axios'
import {bus} from './main'

export default {
    name: 'commonFuncs',
    data() {
        return {}
    },
    mixins: [appMixin]
}

import Vue from 'vue'

export function retrieveAppDictionaries(request_key,paramsArray) {
    let retArray = []
    let apiUrl = process.env.VUE_APP_API_URL

    paramsArray[paramsArray.length]= 'request_key_'+request_key

    axios.post(apiUrl + '/app_settings', paramsArray)
        .then((response) => {
            if (typeof response.data.request_key === 'string') {
                retArray['request_key'] = response.data.request_key
            }

            if (typeof response.data.site_name === 'string') {
                retArray['site_name'] = response.data.site_name
            }

            if (typeof response.data.site_description === 'string') {
                retArray['site_description'] = response.data.site_description
            }

            if (typeof response.data.copyright_text === 'string') {
                retArray['copyright_text'] = response.data.copyright_text
            }

            if (typeof response.data.site_heading === 'string') {
                retArray['site_heading'] = response.data.site_heading
            }

            if (typeof response.data.tasks_per_page === 'string') {
                retArray['tasks_per_page'] = response.data.tasks_per_page
            }

            if (typeof response.data.events_demo_month === 'string') {
                retArray['events_demo_month'] = response.data.events_demo_month
            }

            if (typeof response.data.backend_items_per_page === 'string') {
                retArray['backend_items_per_page'] = response.data.backend_items_per_page
            }

            if (typeof response.data.events_per_page === 'string') {
                retArray['events_per_page'] = response.data.events_per_page
            }

            if (typeof response.data.userTaskTypesLabels === 'object') {
                retArray['userTaskTypesLabels'] = response.data.userTaskTypesLabels
            }

            if (typeof response.data.taskCategoriesLabels === 'object') {
                retArray['taskCategoriesLabels'] = response.data.taskCategoriesLabels
            }

            if (typeof response.data.categoriesLabels === 'object') {
                retArray['categoriesLabels'] = response.data.categoriesLabels
            }

            if (typeof response.data.groupsLabels === 'object') {
                retArray['groupsLabels'] = response.data.groupsLabels
            }

            if (typeof response.data.tasksLabels === 'object') {
                retArray['tasksLabels'] = response.data.tasksLabels
            }

            if (typeof response.data.tags === 'object') {
                retArray['tags'] = response.data.tags
            }

            if (typeof response.data.tasksSearchInfo === 'object') {
                retArray['tasksSearchInfo'] = response.data.tasksSearchInfo
            }


            if (typeof response.data.usersLabels === 'object') {
                retArray['usersLabels'] = response.data.usersLabels
            }

            if (typeof response.data.pollingNotificationCategoryLabels === 'object') {
                retArray['pollingNotificationCategoryLabels'] = response.data.pollingNotificationCategoryLabels
            }

            if (typeof response.data.skillsLabels === 'object') {
                retArray['skillsLabels'] = response.data.skillsLabels
            }

            // console.log('BEFORE appDictionariesRetrieved retArray::')
            // console.log(retArray)

            bus.$emit('appDictionariesRetrieved', retArray)
        })
        .catch((error) => {
            console.error(error)
        })

} // export function retrieveAppDictionaries(request_key, paramsArray, bus) {


Vue.directive('onelementhover', function(el, binding) {

    el.onmouseover = function() {
        if ( binding.value == 'bold' ) {
            el.style.fontWeight = "bold"
        }
        if ( binding.value == 'underline' ) {
            el.style.textDecoration = "underline"
        }
    }
    el.onmouseleave = function() {
        if ( binding.value == 'bold' ) {
            el.style.fontWeight = "normal"
        }
        if ( binding.value == 'underline' ) {
            el.style.textDecoration = "none"
        }
    }
}) // Vue.directive('onelementhover', function(el, binding) {


Vue.directive('checkislogged', function(el, binding, vnode) {
    if ( (typeof binding.value.action_type) && binding.value.action_type == 'grayed' ) {
        if ( binding.value.loggedUser == null ) {
            el.style.color = vnode.context.disabled_text_color
        } else {
            el.style.color = vnode.context.text_color
        }
    }
}) //Vue.directive('checkislogged', function(el, binding, vnode) {
