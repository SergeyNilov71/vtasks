describe('Checking main page and links to other pages of the site', () => {
    it('Open homepage and redirect to first task details page', () => {
        cy.visit('/') // ??
        cy.contains('Select task you want to take participation')
        // cy.pause()
        cy.get('.task_item_container').find('.a_link_details').first().click()

        cy.url()
            .should('include','/task-details/')

        cy.contains('Task details')

        cy.get('.block-wrapper-for-data-listing').then(($legend) => {
            // console.log('$legend::')
            // console.log($legend)
            //
            // alert('$legend.id::' + $legend.id)
            let $nxtElement = $legend.next()
            if ($nxtElement.hasClass('table-wrapper-for-data-listing')  ) {
                // console.log('-1+')
                cy.get('.tr_view_event_details').find('.view_event_details').first().click()
                cy.contains('.modal-title', 'Event Details')
            } else {
                cy.get('p.wrapper-for-no-rows-data-listing').contains('This task has no events yet!')
            }
        })


    })

    it('Open homepage and redirect to first category with tasks list', () => {
        cy.visit('/') // ??
        cy.contains('Select task you want to take participation')
        // cy.pause()

        // cy.find('.a_link_details').click()
        cy.get('.task_item_container').find('.a_link_category_with_tasks_list').first().click()

        cy.url()
            .should('include','/category/')


        cy.contains('Category')


        cy.get('.table-data-listing').find('.a_link_task_details').first().click()
    })

})
