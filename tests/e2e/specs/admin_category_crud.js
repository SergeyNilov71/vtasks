import faker from 'faker'


describe('Admin category CRUD functionality', () => {
    it('Category test', () => {
        // cy.visit('/')

        cy.visit('/login')

        const test_email = 'admin@site.com'
        const test_password = '111111'

        cy.get('#email')
            .clear()
            .type(test_email)
            .should('have.value', test_email)


        cy.get('#password')
            .clear()
            .type(test_password)
            .should('have.value', test_password)


        cy.get('button')
            .contains('Submit')
            .click()

        // cy.wait('.admin_access_rights')
        cy.wait(500)
        // cy.wait('#nav_admin')

        cy.get('article').should('have.class', 'page_content_container')

        cy.get('div.top_nav_menu')
            .find('.admin_access_rights')
            .click()

        cy.visit('/admin/categories') // I want to test CRUD APPS
        cy.get('section').should('have.class', 'listing_header_category')
        cy.get('a.listing_add_button_category')
            .click()


        const faker_name = faker.lorem.word()
        cy.get('#name')
            .clear()
            .type(faker_name)
            .should('have.value', faker_name)


        const faker_content = faker.lorem.word()
        // const faker_content = faker.lorem.sentences()

        cy.get('.editr--content')
            .clear()
            .type(faker_content)

        // cy.get('#name').focus()
        cy.get('#parent_id').click().type('{downarrow}{enter}')


        cy.get('section.submit_form_category')
            .get('button.submit_button_category')
            .click() // by clicking on submit button form is validated and new categoty inserted


        cy.wait(1000)

        cy.url()
            .should('include', '/admin/categories/edit/') // newly inserted row must be redirected to the editor in edit mode with ID in url
            .then((response) => {
                console.log('==response::')
                console.log(response)  // like /admin/categories/edit/52
                 // alert( '-1::' )
                var valuesArray = response.split('/admin/categories/edit/') // get ID of newly inserted category
                if (valuesArray.length == 2) { // got ID of newly inserted category
                    let new_category_id = valuesArray[1]
                    console.log('new_category_id::')
                    console.log(new_category_id)






                    cy.visit('/admin/categories') // I want to test CRUD APPS
                    cy.get('section').should('have.class', 'listing_header_category')


                    cy.get('.a_delete_item_'+new_category_id)
                        .click()

                    cy.get('.a_delete_item_'+new_category_id).should('have.value', 'Do you want to delete \''+faker_name+'\' category ?')
                    // alert('new_category_id::' + new_category_id)
                }
            })
/*
        cy.contains('Edit category')

        cy.get('section.submit_form_category')
            .get('a.cancel_button_category')
            .click()


        cy.url()
            .should('include', '/admin/categories')
        cy.contains('Categories')

        cy.get('section').should('have.class', 'listing_header_category')
        console.log('new_category_id::')
        console.log(new_category_id)

        cy.get('.a_delete_item_'+new_category_id)
            .click()
*/

    })

})

