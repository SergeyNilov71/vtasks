import faker from 'faker'
describe('Checking register/login functionality of the site', () => {
    it('Open homepage and redirect to register', () => {

        // Alias the route to wait for its response
        cy.server()
        cy.route('GET', '/login').as('getLogin')


        cy.visit('/')
        cy.contains('Select task you want to take participation')
        cy.get('#nav').find('.a_link_top_menu_register').first().click()

        cy.url()
            .should('include','/register')
        cy.contains('Register')

        const faker_email= faker.internet.email()
        const faker_password= faker.internet.password()
        const faker_username= faker.name.findName()
        cy.get('#name')
            .clear()
            .type(faker_username)
            .should('have.value', faker_username)
        cy.get('#email')
            .clear()
            .type(faker_email)
            .should('have.value', faker_email)

        cy.get('#password')
            .clear()
            .type(faker_password)
            .should('have.value', faker_password)

        cy.get('#password_confirmation')
            .clear()
            .type(faker_password)
            .should('have.value', faker_password)

        cy.get('button')
            .contains('Submit')
            .click()


        // cy.wait('@getLogin').its('status').should('eq', 200)

        cy.wait(1000)


        /* Check invalid login */
        cy.url()
            .should('include','/login')
        cy.get('#email')
            .clear()
            .type('invalid_email_'+faker_email)
            .should('have.value', 'invalid_email_'+faker_email)
        cy.get('#password')
            .clear()
            .type(faker_password)
            .should('have.value', faker_password)
        cy.get('button')
            .contains('Submit')
            .click()
        cy.url() // must stayed at the same page
            .should('include','/login')


        /* Check valid login */
        cy.url()
            .should('include','/login')
        cy.get('#email')
            .clear()
            .type(faker_email)
            .should('have.value', faker_email)
        cy.get('#password')
            .clear()
            .type(faker_password)
            .should('have.value', faker_password)
        cy.get('button')
            .contains('Submit')
            .click()


        cy.get('#nav').find('.a_link_top_menu_profile').first().click()
        cy.url()
            .should('include','/profile')
        cy.contains('Profile of')


        cy.get('#nav').find('.a_link_top_menu_logout').first().click()
        cy.url()
            .should('include','/login')

    })


})
