import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Tasks from '../views/tasks/TasksPage.vue'
import store from '../store'
import MaintenanceMessage from '../views/MaintenanceMessage.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/MaintenanceMessage',
    name: 'MaintenanceMessage',
    component: MaintenanceMessage
  },

  {
    path: '/',
    name: 'home',
    component: Home
  },

  {
    path: '/tasks',
    name: 'tasksPage',
    component: Tasks
  },

  {
    path: '/task-details/:slug',
    name: 'taskDetails',
    component: () => import( '../views/tasks/TaskDetails.vue')
  },
  {
    path: '/category/:slug',
    name: 'category',
    component: () => import( '../views/category/Category.vue')
  },

  {
    path: '/about',
    name: 'about',
    component: () => import( '../views/About.vue')
  },

  {
    path: '/test',
    name: 'test',
    component: () => import( '../views/Test.vue')
  },

  {
    path: '/test2',
    name: 'test2',
    component: () => import( '../views/Test2.vue')
  },

  {
    path: '/test3',
    name: 'test3',
    component: () => import( '../views/Test3.vue')
  },

  {
    path: '/test4',
    name: 'test4',
    component: () => import( '../views/Test4.vue')
  },

  {
    path: '/design_blocks',
    name: 'design_blocks',
    component: () => import( '../views/designBlocks.vue')
  },

  {
    path: '/login',
    name: 'login',
    component: () => import('../views/auth/Login.vue')
  },

  {
    path: '/register',
    name: 'register',
    component: () => import('../views/auth/Register.vue')
  },
  {
    path: '/activate/:code/signup',
    name: 'activate',
    component: () => import('../views/auth/Activation.vue')    // src/views/auth/Activation.vue
  },

  {
    path: '/forgot_password',
    name: 'forgot_password',
    component: () => import('../views/auth/ForgotPassword.vue')
  },
  { // http://local-ctasks-api.com/password-reset/HNMj
    path: '/password-reset/:code/make',
    name: 'passwordReset',
    component: () => import('../views/auth/PasswordReset.vue')
  }, // i.com/api/password-reset/create


    // http://localhost:8080/login

  {
    path: '/activation/:code',
    name: 'activation',
    component: () => import('../views/auth/Activation.vue')
  },

  {
    path: '/profile',
    name: 'profile',
    component: () => import('../views/personal/profile.vue'),
    meta: {
      requiresAuth: true
    }
  },

  {
    path: '/user-todos',
    name: 'userTodos',
    component: () => import('../views/personal/UserTodos.vue'),  // src/views/personal/UserTodos.vue
    meta: {
      requiresAuth: true
    }
  },

  {
    path: '/weather-viewer',
    name: 'weatherViewer',
    component: () => import('../views/personal/WeatherViewer.vue'),
    meta: {
      requiresAuth: true
    }
  },

  {
    path: '/profile-editor',
    name: 'profileEditor',
    component: () => import('../views/personal/profileEditor.vue'),
    meta: {
      requiresAuth: true
    }
  },

  {
    path: '/public_profile/:id',
    name: 'publicProfile',
    component: () => import('../views/publicProfile.vue'),
  },


  {
    path: '/not-found/:invalid_url?',
    name: 'notFound',
    component: () => import('../views/NotFound.vue')
  },

  {
    path: '/events',
    name: 'eventsTimeline',
    component: () => import('../views/events/eventsTimeline.vue')
  },



    // ADMIN'S CRUD BLOCK START
  {
    path: '/admin/dashboard',
    name: 'adminDashboard',
    component: () => import('../views/admin/dashboard/dashboard.vue'),
    meta: {
      requiresAuth: true
    }
  },

  {
    path: '/admin/settings',  // src/views/admin/dashboard/settings.vue
    name: 'settings',
    component: () => import('../views/admin/dashboard/settings.vue'),
    meta: {
      requiresAuth: true
    }
  },


  {
    path: '/admin/skills',
    name: 'adminSkillsListing',
    component: () => import('../views/admin/skills/list.vue'),
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/admin/skills/edit/:id',
    name: 'adminSkillEditor',
    component: () => import('../views/admin/skills/editor.vue'),
    meta: {
      requiresAuth: true
    }
  },



  {
    path: '/admin/tags',
    name: 'adminTagsListing',
    component: () => import('../views/admin/tags/list.vue'),
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/admin/tags/edit/:id',
    name: 'adminTagEditor',
    component: () => import('../views/admin/tags/editor.vue'),
    meta: {
      requiresAuth: true
    }
  },



  {
    path: '/admin/activity',
    name: 'adminActivityListing',
    component: () => import('../views/admin/activity/list.vue'),
    meta: {
      requiresAuth: true
    }
  },




  {
    path: '/admin/events',
    name: 'adminEventsListing',
    component: () => import('../views/admin/events/list.vue'),
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/admin/events/edit/:id',
    name: 'adminEventEditor',
    component: () => import('../views/admin/events/editor.vue'),     // src/views/admin/events/editor.vue
    meta: {
      requiresAuth: true
    }
  },


  {
    path: '/admin/polling-notifications',
    name: 'adminPollingNotificationsListing',
    component: () => import('../views/admin/polling_notifications/list.vue'), // src/views/admin/polling_notifications/list.vue
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/admin/polling-notifications/edit/:id',
    name: 'adminPollingNotificationEditor',
    component: () => import('../views/admin/polling_notifications/editor.vue'),
    meta: {
      requiresAuth: true
    }
  },


  {
    path: '/admin/user-task-types',
    name: 'adminUserTaskTypesListing',
    component: () => import('../views/admin/user_task_types/list.vue'),
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/admin/user-task-types/edit/:id',
    name: 'adminUserTaskTypeEditor',
    component: () => import('../views/admin/user_task_types/editor.vue'),
    meta: {
      requiresAuth: true
    }
  },

  {
    path: '/admin/categories',
    name: 'adminCategoriesListing',
    component: () => import('../views/admin/categories/list.vue'),
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/admin/categories/edit/:id',
    name: 'adminCategoryEditor',
    component: () => import('../views/admin/categories/editor.vue'),
    meta: {
      requiresAuth: true
    }
  },


    /*     path: '/admin/tasks/:additive_action?/:additive_action_id?',
    name: 'adminTasksListing',
    component: () => import('../views/admin/tasks/list.vue'),
    meta: {
      requiresAuth: true
    }
  },

 */
  {
    path: '/admin/users/:additive_action?/:additive_action_id?',
    name: 'adminUsersListing',
    component: () => import('../views/admin/users/list.vue'),
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/admin/users/edit/:id',
    name: 'adminUserEditor', //adminUserEditor
    component: () => import('../views/admin/users/editor.vue'),
    meta: {
      requiresAuth: true
    }
  },

  {
    path: '/admin/tasks/edit/:id',
    name: 'adminTaskEditor',
    component: () => import('../views/admin/tasks/editor.vue'),
    meta: {
      requiresAuth: true
    }
  },


  { //                 path: ':id/:additive_action?/:additive_action_id?',
    path: '/admin/tasks/:additive_action?/:additive_action_id?',
    name: 'adminTasksListing',
    component: () => import('../views/admin/tasks/list.vue'),
    meta: {
      requiresAuth: true
    }
  },


  // ADMIN'S CRUD BLOCK END



]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (store.getters.isLoggedIn) {
      next()
      return
    }
    next('/login')
  } else {

    if (!to.matched.length) {
      next(  '/not-found/'+encodeURIComponent(to.path)  )
    } else {
      next()
    }

  }
})
export default router
