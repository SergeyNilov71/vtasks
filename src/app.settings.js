export const settingsUsersAssignedToTaskActionLabels = [
    { code: 'A', label: 'Accept', info: 'User can start working in the task' },
    { code: 'C', label: 'Cancel', info: 'User can not work in the task, but the request is not deleted and can be accepted later' },
    { code: 'R', label: 'Remove', info: 'User can not work in the task, and the request is deleted' }
]

export const settingsYesNoLabels = [
    { code: 0, label: 'No' },
    { code: 1, label: 'Yes' }
]


// nextPollingNotification.type
export const settingsPollingNotificationTypeLabels = [
    { code: 'O', label: 'Show once' },
    { code: 'C', label: 'Show once and confirmed' },
    { code: 'R', label: 'Repeatedly' },
]

export const settingsPollingNotificationStyleLabels = [
    { code: 'I', label: 'Info' },
    { code: 'W', label: 'Warning' },
    { code: 'D', label: 'Danger' },
]

export const settingsPollingNotificationActiveLabels = [
    { code: 1, label: 'Active' },
    { code: 0, label: 'Inactive' },
]


export const settingsAwesomeFontLabels = [   // https://fontawesome.com/v4.7.0/icons/
    { code: 'home', font: 'fa fa-home' },
    { code: 'task', font: 'fa fa-tasks' },
    { code: 'task_category', font: 'fa fa-object-group' },
    { code: 'server', font: 'fa fa-server' },
    { code: 'user_task_type', font: 'fa fa-id-badge' },
    { code: 'member_of_task', font: 'fa fa-user-plus ' },
    { code: 'member_of_task_2x', font: 'fa fa-2x fa-user-plus' },
    { code: 'money', font: 'fa fa-money' },
    { code: 'free', font: 'fa fa-circle-o-notch' },
    { code: 'subscription', font: 'fa fa-envelope-open-o' },
    { code: 'email', font: 'fa fa-envelope' },
    { code: 'password', font: 'fa fa-unlock-alt' },
    { code: 'filter', font: 'fa fa-filoter' },
    { code: 'key', font: 'fa fa-key' },
    { code: 'cancel', font: 'fa fa-window-close' },
    { code: 'save', font: 'fa fa-save' },
    { code: 'login', font: 'fa fa-sign-in' },
    { code: 'register', font: 'fa fa-registered' },
    { code: 'profile', font: 'fa fa-user-md' },
    { code: 'users', font: 'fa fa-users' },
    { code: 'user', font: 'fa fa-user' },
    { code: 'needs_reports', font: 'fa fa-flag' },
    { code: 'leader', font: 'fa fa-magnet' },
    { code: 'priority', font: 'fa fa-asterisk' },
    { code: 'priority_2x', font: 'fa fa-2x fa-asterisk' },
    { code: 'status', font: 'fa fa-battery-half' },
    { code: 'status_2x', font: 'fa fa-2x fa-battery-half' },
    { code: 'event', font: 'fa fa-calendar' },
    { code: 'events', font: 'fa fa-calendar' },
    { code: 'tag', font: 'fa fa-tag' },
    { code: 'edit', font: 'fa fa-edit' },
    { code: 'published', font: 'fa fa-unlink' },
    { code: 'description', font: 'fa fa-info-circle' },
    { code: 'past_date', font: 'fa fa-podcast' },
    { code: 'view', font: 'fa fa-eye' },
    { code: 'view_crossed_out', font: 'fa fa-eye-slash' },
    { code: 'tools', font: 'fa fa-wrench' },
    { code: 'check', font: 'fa fa-check' },
    { code: 'check-circle', font: 'fa fa-check-circle' },
    { code: 'check-square', font: 'fa fa-check-square' },
    { code: 'admin', font: 'fa fa-snapchat-ghost' },
    { code: 'manager', font: 'fa fa-graduation-cap' },
    { code: 'developer', font: 'fa fa-wheelchair-alt' },
    { code: 'has_no_access', font: 'fa fa-key' },
    { code: 'has_access', font: 'fa fa-unlock-alt' },
    { code: 'action', font: 'fa fa-exclamation-triangle' },
    { code: 'remove', font: 'fa fa-trash' },
    { code: 'remove2x', font: 'fa fa-2x fa-trash' },
    { code: 'unremove', font: 'fa fa-building-o' },
    { code: 'unremove2x', font: 'fa fa-2x fa-building-o' },
    { code: 'demo-info', font: 'fa fa-info-circle' },
    { code: 'skill', font: 'fa fa-grav' },
    { code: 'dashboard', font: 'fa fa-th-large' },
    { code: 'settings', font: 'fa fa-cog' },
    { code: 'upload', font: 'fa fa-upload' },
    { code: 'attach', font: 'fa fa-address-book' },
    { code: 'select', font: 'fa fa-mouse-pointer' },
    { code: 'toggle_on', font: 'fa fa-arrows-alt' },
    { code: 'weather', font: 'fa fa-sun-o' },
    { code: 'todo', font: 'fa-exclamation-circle' },
    { code: 'thunder', font: 'fa fa-bolt' },
    { code: 'today', font: 'fa fa-calendar-o' },
    { code: 'next_days', font: 'fa fa-calendar-check-o' },

    { code: 'toggle_off', font: 'fa fa-arrow-up' },
    { code: 'search', font: 'fa fa-search' },
    { code: 'activity_log', font: 'fa fa-table' },

    {code: 'export', font: 'fa fa-cubes'},
    {code: 'import', font: 'fa fa-database'},

    {code: 'maintenance', font: 'fa fa-truck'},
    {code: 'polling_notification', font: 'fa fa-sticky-note-o'},
    {code: 'notification', font: 'fa fa-sticky-note-o'},
    {code: 'tree_has_items', font: 'fa fa-angle-right'},
    {code: 'lat_lan', font: 'fa fa-arrows'},
    { code: 'test', font: 'fa fa-bug' }
]

export const settingsTaskPriorityLabels = [
    { code: '0', label: 'No' },
    { code: '1', label: 'Low' },
    { code: '2', label: 'Medium' },
    { code: '3', label: 'High' },
    { code: '4', label: 'Urgent' },
    { code: '5', label: 'Immediate' }
]

export const settingsTaskNeedsReportsLabels = [
    { code: '0', label: 'No' },
    { code: '1', label: 'Hourly' },
    { code: '2', label: 'Twice a day' },
    { code: '3', label: 'Daily' },
    { code: '4', label: 'Twice a week' },
    { code: '5', label: 'Weekly' }
]

export const settingsTaskIsHomepageLabels = [
    { code: 1, label: 'Is Homepage' },
    { code: 0, label: 'Is Not Homepage' },
]


export const settingsShowDeletedTasksLabels = [
    { code: 0, label: 'Show only non deleted tasks' },
    { code: 1, label: 'Show only deleted tasks' },
]

export const settingsUserSkillsLabels = [
    { code: 0, label: 'No skills' },
    { code: 1, label: 'Very few' },
    { code: 2, label: 'Rather few' },
    { code: 3, label: 'Few medium' },
    { code: 4, label: 'Medium' },
    { code: 5, label: 'Better medium' },
    { code: 6, label: 'Rather good' },
    { code: 7, label: 'Good' },
    { code: 8, label: 'Very good' },
    { code: 9, label: 'Excellent' },
    { code: 10, label: 'Guru' }
]

export const settingsTaskStatusLabels = [
    { code: 'D', label: 'Draft' },
    { code: 'A', label: 'Assigning' },
    { code: 'C', label: 'Cancelled' },
    { code: 'P', label: 'Processing' },
    { code: 'K', label: 'Checking' },
    { code: 'O', label: 'Completed' }
]

export const settingsTaskAssignedToUsersStatusLabels = [
    { code: 'A', label: 'Accepted' },
    { code: 'C', label: 'Cancelled' },
    { code: 'N', label: 'New(Waiting for acception)' }
]

export const settingsEventAccessLabels = [
    { code: 'P', label: 'Private' },
    { code: 'U', label: 'Public' }
]

export const settingsTagTypeLabels = [
    { code: 'E', label: 'Events' },
    { code: 'P', label: 'Pages' },
    { code: 'T', label: 'Tasks' }
]

export const settingsUserStatusLabels = [
    { code: 'A', label: 'Active' },
    { code: 'I', label: 'Inactive' },
    { code: 'N', label: 'New' }
]

export const settingsUserStatusOnRegistrationLabels = [
    { code: 'N', label: 'New ( needs activation )' },
    { code: 'A', label: 'Active ( Can login at once )' },
]

export const settingShowPollingNotificationsLabels = [
    { code: 'Y', label: 'Show Polling Notifications' },
    { code: 'N', label: 'Not Show Polling Notifications' },
]

export const settingsJsMomentDatetimeFormat = 'Do MMMM, YYYY h:mm A'
export const settingsJsMomentDateFormat = 'Do MMMM, YYYY'
export const settingsEmptyImg = '/images/emptyImg.png'

export const settingsDefaultPollingNotificationImg = '/images/polling-notification.png'
export const settingsDefaultTaskImg = '/images/task.jpg'
export const settingsDefaultCategoryImg = '/images/defaultCategory.jpeg'
export const settingsDefaultPollingNotificationsImg = '/images/polling_notifications.jpg'

export const settingsLogoImg = '/images/logo.png'
export const settingsLogoBigImg = '/images/logo_big.png'

export const settingsAdminLogoImg = '/images/admin_logo.png'
export const settingsAdminLogoBigImg = '/images/admin_logo_big.png'

export const settingsPriorityAsapRange = 4

export const settingCredentialsConfig = {
    withCredentials:true,
    headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials':true
    }
}
