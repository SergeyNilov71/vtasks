import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'

import 'bootstrap'
import 'font-awesome/css/font-awesome.css'

import VModal from 'vue-js-modal'
Vue.use(VModal)

import Notifications from 'vue-notification'
Vue.use(Notifications)

import moment from 'moment-timezone'
moment.tz.setDefault(process.env.VUE_APP_TIME_ZONE)

import Vue2Filters from 'vue2-filters'
Vue.use(Vue2Filters)


export const bus = new Vue()

axios.defaults.crossDomain = true
let apiUrl = process.env.VUE_APP_API_URL

Vue.config.productionTip = false

Vue.prototype.$http = axios

const token = localStorage.getItem('token')
if (token) {
    Vue.prototype.$http.defaults.headers.common['Authorization'] = token
}

new Vue({
    router,
    store,
    bus,
    render: h => h(App)
}).$mount('#app')

Vue.use({
    install (Vue) {
        Vue.prototype.$api = axios.create({
            baseURL: apiUrl
        })
    }
})

router.afterEach((to, from) => {
    bus.$emit('page_changed', from, to)
})
