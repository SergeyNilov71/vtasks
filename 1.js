var this_is_page_loaded
var this_backend_home_url
var this_id
var this_add_new_client_selected_existing= true
var this_current_check_in_step = ''
var this_current_reserve_step = ''
var this_csrf_token
var this_current_currency_short
var this_current_currency_position
var this_colorValuesArray


function bookingsAndAvailability(page, paramsArray) {  // constructor of backend bookingsAndAvailability listing - set all from referring page and from server
    this_is_page_loaded = false;
    this_backend_home_url = paramsArray.backend_home_url;
    this_backendLengthMenuArray = paramsArray.backendLengthMenuArray;
    this_csrf_token = paramsArray.csrf_token;

    this_current_currency_short = paramsArray.current_currency_short;
    this_current_currency_position = paramsArray.current_currency_position;

    this_colorValuesArray = paramsArray.colorValuesArray;
    this_colorValuesArray = convertObjectToArray(paramsArray.colorValuesArray);

    if (page == "edit") {
        this_id = paramsArray.id;

        $('#service_ids').select2({    // multi select implementation from https://select2.org
            placeholder: "Select service(s)",
            allowClear: true
        });
    }
    if (page == "list") {
        this.bookingsAndAvailabilityDataLoad()
        $(".dataTables_filter").css("display", "none")
        $(".dataTables_info").css("display", "none")
        $('#filter_service_ids').select2({   // multi select implementation from https://select2.org
            placeholder: " -Select service(s)- ",
            allowClear: true
        });   //https://select2.github.io/select2-bootstrap-theme/4.0.3.html
        $(document).keypress(function (event) {
            if (event.keyCode == 13) {
                $('#btn_run_search').click();
            }
        });


        new AutoNumeric('#numeric_check_out_vat', {
            currencySymbolPlacement: this_current_currency_position,
            currencySymbol: this_current_currency_short,
            maximumValue: '9999999999.99',
            minimumValue: 0.00
        });
        const check_out_vatAutoNumeric = document.querySelector('#numeric_check_out_vat');
        check_out_vatAutoNumeric.addEventListener('autoNumeric:rawValueModified', e => {
            if (typeof e.detail.newRawValue != "undefined") {
                $("#check_out_vat").val(e.detail.newRawValue)
            }
        });

        new AutoNumeric('#numeric_check_insurance_vat', {
            currencySymbolPlacement: this_current_currency_position,
            currencySymbol: this_current_currency_short,
            maximumValue: '9999999999.99',
            minimumValue: 0.00
        });
        const check_insurance_vatAutoNumeric = document.querySelector('#numeric_check_insurance_vat');
        check_insurance_vatAutoNumeric.addEventListener('autoNumeric:rawValueModified', e => {
            if (typeof e.detail.newRawValue != "undefined") {
                $("#check_insurance_vat").val(e.detail.newRawValue)
            }
        });


        new AutoNumeric('#numeric_check_om_vat', {
            currencySymbolPlacement: this_current_currency_position,
            currencySymbol: this_current_currency_short,
            maximumValue: '9999999999.99',
            minimumValue: 0.00
        });
        const check_om_vatAutoNumeric = document.querySelector('#numeric_check_om_vat');
        check_om_vatAutoNumeric.addEventListener('autoNumeric:rawValueModified', e => {
            if (typeof e.detail.newRawValue != "undefined") {
                $("#check_om_vat").val(e.detail.newRawValue)
            }
        });


        new AutoNumeric('#numeric_check_electricity_vat', {
            currencySymbolPlacement: this_current_currency_position,
            currencySymbol: this_current_currency_short,
            maximumValue: '9999999999.99',
            minimumValue: 0.00
        });
        const check_electricity_vatAutoNumeric = document.querySelector('#numeric_check_electricity_vat');
        check_electricity_vatAutoNumeric.addEventListener('autoNumeric:rawValueModified', e => {
            if (typeof e.detail.newRawValue != "undefined") {
                $("#check_electricity_vat").val(e.detail.newRawValue)
            }
        });


        new AutoNumeric('#numeric_check_internet_vat', {
            currencySymbolPlacement: this_current_currency_position,
            currencySymbol: this_current_currency_short,
            maximumValue: '9999999999.99',
            minimumValue: 0.00
        });
        const check_internet_vatAutoNumeric = document.querySelector('#numeric_check_internet_vat');
        check_internet_vatAutoNumeric.addEventListener('autoNumeric:rawValueModified', e => {
            if (typeof e.detail.newRawValue != "undefined") {
                $("#check_internet_vat").val(e.detail.newRawValue)
            }
        });


        new AutoNumeric('#numeric_check_racks_vat', {
            currencySymbolPlacement: this_current_currency_position,
            currencySymbol: this_current_currency_short,
            maximumValue: '9999999999.99',
            minimumValue: 0.00
        });
        const check_racks_vatAutoNumeric = document.querySelector('#numeric_check_racks_vat');
        check_racks_vatAutoNumeric.addEventListener('autoNumeric:rawValueModified', e => {
            if (typeof e.detail.newRawValue != "undefined") {
                $("#check_racks_vat").val(e.detail.newRawValue)
            }
        });


        new AutoNumeric('#numeric_check_hot_desk_vat', {
            currencySymbolPlacement: this_current_currency_position,
            currencySymbol: this_current_currency_short,
            maximumValue: '9999999999.99',
            minimumValue: 0.00
        });
        const check_hot_desk_vatAutoNumeric = document.querySelector('#numeric_check_hot_desk_vat');
        check_hot_desk_vatAutoNumeric.addEventListener('autoNumeric:rawValueModified', e => {
            if (typeof e.detail.newRawValue != "undefined") {
                $("#check_hot_desk_vat").val(e.detail.newRawValue)
            }
        });


        if ($('#check_in_datepicker').length) {
            $('#check_in_datepicker').datepicker({    // bootstrap 4/ fontawesome compatible datepicker from https://gijgo.com/datepicker
                uiLibrary: 'bootstrap4',
                iconsLibrary: 'fontawesome',
                showOtherMonths: true,
                selectOtherMonths: true,
                format: 'dd mmmm, yyyy'
            });
        }
        if ($('#check_out_datepicker').length) {
            $('#check_out_datepicker').datepicker({
                uiLibrary: 'bootstrap4',
                iconsLibrary: 'fontawesome',
                showOtherMonths: true,
                selectOtherMonths: true,
                format: 'dd mmmm, yyyy'
            });
        }


        if ($('#reserve_datepicker').length) {
            $('#reserve_datepicker').datepicker({
                uiLibrary: 'bootstrap4',
                iconsLibrary: 'fontawesome',
                showOtherMonths: true,
                selectOtherMonths: true,
                format: 'dd mmmm, yyyy'
            });
        }
        if ($('#reserve_out_datepicker').length) {
            $('#reserve_out_datepicker').datepicker({
                uiLibrary: 'bootstrap4',
                iconsLibrary: 'fontawesome',
                showOtherMonths: true,
                selectOtherMonths: true,
                format: 'dd mmmm, yyyy'
            });
        }


        $('#checkout_out_datepicker').datepicker({
            uiLibrary: 'bootstrap4',
            iconsLibrary: 'fontawesome',
            showOtherMonths: true,
            selectOtherMonths: true,
            format: 'dd mmmm, yyyy'
        });

    }
    this_is_page_loaded = true;
} // function bookingsAndAvailability(Params) {  constructor of backend bookingsAndAvailability listing - set all from referring page and from server


bookingsAndAvailability.prototype.onBackendPageInit = function (page) {  // all vars/objects init
    backendInit()
} // bookingsAndAvailability.prototype.onBackendPageInit= function(page) {


bookingsAndAvailability.prototype.runSearch = function (oTable) {
    oTable.draw();
}


bookingsAndAvailability.prototype.bookingsAndAvailabilityDataLoad = function () {
    Mustache.tags = ["<%", "%>"];

    var columnsData = []
    columnsData[columnsData.length] = {data: 'id', name: 'id'}
    columnsData[columnsData.length] = {data: 'number', name: 'number'}
    columnsData[columnsData.length] = {data: 'selling_range', name: 'selling_range'}
    columnsData[columnsData.length] = {data: 'actual_storage_rent', name: 'actual_storage_rent'}
    columnsData[columnsData.length] = {data: 'insurance_vat', name: 'insurance_vat'}
    columnsData[columnsData.length] = {data: 'warehouse_name', name: 'warehouse_name'}
    columnsData[columnsData.length] = {data: 'notes', name: 'notes'}
    columnsData[columnsData.length] = {data: 'storage_capacity_name', name: 'storage_capacity_name'}

    columnsData[columnsData.length] = {data: 'client_full_name', name: 'client_full_name'}

    columnsData[columnsData.length] = {data: 'job_ref_no', name: 'job_ref_no'}
    columnsData[columnsData.length] = {data: 'check_in_date', name: 'check_in_date'}
    columnsData[columnsData.length] = {data: 'check_out_date', name: 'check_out_date'}
    columnsData[columnsData.length] = {data: 'status', name: 'status'}

    columnsData[columnsData.length] = {data: 'action', name: 'action', orderable: false, searchable: false}
    columnsData[columnsData.length] = {data: 'action_delete', name: 'action_delete', orderable: false, searchable: false}


    oTable = $('#get-bookings-and-availability-dt-listing-table').DataTable({
        processing: true,
        autoWidth: false,
        language: {
            "processing": "Loading Bookings And Availability..."
        },
        serverSide: true,
        "lengthChange": true,
        "lengthMenu": this_backendLengthMenuArray,
        ajax: {
            url: this_backend_home_url + '/admin/get-bookings-and-availability-dt-listing',
            data: function (d) {
                d.filter_status = $("#filter_status").val();
                d.filter_location_id = $("#filter_location_id").val();
                d.filter_level = $("#filter_level").val();
                d.filter_storage_capacity_id = $("#filter_storage_capacity_id").val();
                d.filter_capacity_category_id = $("#filter_capacity_category_id").val();
                d.filter_client_id = $("#filter_client_id").val();
                d.filter_name = $("#filter_name").val();
                d.filter_color_id = $("#filter_color_id").val();
                d.filter_search = $("#filter_search").val();
                d.filter_service_ids = $("#filter_service_ids").val();
                d.filter_number = $("#filter_number").val();
            },
        }, // ajax: {

        columns: columnsData,

        "drawCallback": function (settings, b) {
            // bookingsAndAvailability.openCheckInStorageSpaceDialog(2, 'S1-102', true) // DEBUGGING
            bookingsAndAvailability.openReserveStorageSpaceDialog(2,'S1-102', true)
            var span_bookings_and_availabilities_records_count_content = $("#span_bookings_and_availabilities_records_count").html()

            if (typeof span_bookings_and_availabilities_records_count_content != "undefined") {
                $("#span_bookings_and_availabilities_records_count").html(". Shows " + settings.json.data.length + " of " + settings.json.recordsFiltered + " box rooms")
            } else {
                var $label = $("<label>").text(". Shows " + settings.json.data.length + " of " + settings.json.recordsFiltered + " box rooms").attr({
                    id: 'span_bookings_and_availabilities_records_count',
                    name: 'span_bookings_and_availabilities_records_count'
                });

                $(".dataTables_length > label ").append($label);
            }

            $(".dataTables_info").css("display", "none")
            if (settings.json.recordsTotal <= settings.aiDisplay.length) { // we need to hide pagination block
                $(".dataTables_paginate").css("display", "none")
            } else {  // we need to show pagination block
                $(".dataTables_paginate").css("display", "block")
            }

        },

    }); // oTable = $('#get-bookings-and-availability-dt-listing-table').DataTable({

}


bookingsAndAvailability.prototype.deleteStorageSpace = function (id, number) {
    confirmMsg('Do you want to delete "' + number + '" box room with all related data ?', function () {
            var href = this_backend_home_url + "/admin/box-rooms/destroy";
            $.ajax({
                type: "DELETE",
                dataType: "json",
                url: href,
                data: {"id": id, "_token": this_csrf_token},
                success: function (response) {
                    $("#btn_run_search").click()
                    popupAlert("Box room was successfully deleted !", 'success')
                },
                error: function (error) {
                    popupErrorMessage(error.responseJSON.message)
                }
            });

        }
    );

} // bookingsAndAvailability.prototype.deleteStorageSpace = function ( id, number ) {


bookingsAndAvailability.prototype.capacity_category_idOnChange = function () {
    if (!this_is_page_loaded) {
        return;
    }
    let capacity_category_id = $("#filter_capacity_category_id").val();
    var href = this_backend_home_url + "/admin/get-storage-capacities-by-capacity-category-id/" + capacity_category_id;

    $.ajax({
        type: "GET",
        dataType: "json",
        url: href,
        success: function (response) {
            $('.chosen_filter_storage_capacity_id').empty().append('<option value=""> -Select Unit Size- </option>');
            $('.chosen_filter_storage_capacity_id').trigger("chosen:updated");

            response.list.forEach(function (item) {
                $('.chosen_filter_storage_capacity_id').append('<option value="' + item.id + '">' + item.count + '</option>');
            });
            $('.chosen_filter_storage_capacity_id').trigger("chosen:updated");
        },
        error: function (error) {
            popupErrorMessage(error.responseJSON.message)
        }
    });
}


bookingsAndAvailability.prototype.storageSpaceShowFees = function (id, number, status, currency, selling_range, insurance_vat, om_vat, electricity_vat, internet_vat, racks_vat, hot_desk_vat) {

    if (status != 'B') {
        popupAlert('Fees are only applicable for booked and paid room !', 'danger')
        return;
    }
    $("#div_storage_space_show_fees_modal").modal({
        "backdrop": "static",
        "keyboard": true,
        "show": true
    });
    if (insurance_vat == null || typeof insurance_vat == "undefined" || jQuery.trim(insurance_vat) == "") insurance_vat = 0
    if (typeof insurance_vat == 'string') {
        insurance_vat = parseFloat(insurance_vat)
    }

    if (om_vat == null || typeof om_vat == "undefined" || jQuery.trim(om_vat) == "") om_vat = 0
    if (typeof om_vat == 'string') {
        om_vat = parseFloat(om_vat)
    }

    if (electricity_vat == null || typeof electricity_vat == "undefined" || jQuery.trim(electricity_vat) == "") electricity_vat = 0
    if (typeof electricity_vat == 'string') {
        electricity_vat = parseFloat(electricity_vat)
    }

    if (internet_vat == null || typeof internet_vat == "undefined" || jQuery.trim(internet_vat) == "") internet_vat = 0
    if (typeof internet_vat == 'string') {
        internet_vat = parseFloat(internet_vat)
    }

    if (racks_vat == null || typeof racks_vat == "undefined" || jQuery.trim(racks_vat) == "") racks_vat = 0
    if (typeof racks_vat == 'string') {
        racks_vat = parseFloat(racks_vat)
    }

    if (hot_desk_vat == null || typeof hot_desk_vat == "undefined" || jQuery.trim(hot_desk_vat) == "") hot_desk_vat = 0
    if (typeof hot_desk_vat == 'string') {
        hot_desk_vat = parseFloat(hot_desk_vat)
    }

    $('#storage_space_show_fees_insurance_vat').val(currency + insurance_vat.toFixed(2))
    $('#storage_space_show_fees_om_vat').val(currency + om_vat.toFixed(2))
    $('#storage_space_show_fees_electricity_vat').val(currency + electricity_vat.toFixed(2))
    $('#storage_space_show_fees_internet_vat').val(currency + internet_vat.toFixed(2))
    $('#storage_space_show_fees_racks_vat').val(currency + racks_vat.toFixed(2))
    $('#storage_space_show_fees_hot_desk_vat').val(currency + hot_desk_vat.toFixed(2))
    $('#span_storage_space_show_fees_number').html(number)

} // bookingsAndAvailability.prototype.storageSpaceShowFees = function ( id, number ) {

////////// bookingsAndAvailability Block End ///////////

// SET STORAGE COLORS BLOCK START

// SET STORAGE COLORS BLOCK END


// STORAGE SPACE CHECK OUT BLOCK START
bookingsAndAvailability.prototype.openCheckOutStorageSpace = function (id, number) {
    var href = this_backend_home_url + "/admin/get-box-room-status-info-by-id/" + id;
    $.ajax({
        type: "get",
        dataType: "json",
        url: href,
        success: function (response) {
            // console.log('response::')
            // console.log(response)

            if (typeof response.storageSpaceCheckIn == "undefined") {
                $('#checkout_by_client_full_name').val("")
                $('#checkout_vat').val("")
                $('#checkout_date').val("")
                $('#checkout_out_date').val("")
            } else {
                $('#checkout_by_client_full_name').val(response.storageSpaceCheckIn.booked_by_client_full_name)
                $('#checkout_vat').val(response.storageSpaceCheckIn.vat)
                $('#checkout_date').val(response.storageSpaceCheckIn.booked_date)
                $('#checkout_out_date').val(response.storageSpaceCheckIn.booked_out_date)
            }
            $('#checkout_out_date').val("")
            $("#div_checkout_storage_space_modal").modal({
                "backdrop": "static",
                "keyboard": true,
                "show": true
            });
            $('#checkout_storage_space_id').val(id)
            $('#span_checkout_number').html(number)
        },
        error: function (error) {
            popupErrorMessage(error.responseJSON.message)
        }
    });

} // bookingsAndAvailability.prototype.openCheckOutStorageSpace = function ( id, number ) {


bookingsAndAvailability.prototype.checkOutStorageSpace = function () {
    var checkout_storage_space_id = $('#checkout_storage_space_id').val()

    var checkout_out_datepicker = $('#checkout_out_datepicker').val()
    if (typeof checkout_out_datepicker == "undefined" || checkout_out_datepicker == "") {
        popupAlert("Select check out date !", 'danger')
        $('#checkout_out_datepicker').focus()
        return;
    }

    var check_out_date = new Date(checkout_out_datepicker.replace(/,/gm, ''))
    var href = this_backend_home_url + "/admin/check-out/make";
    $.ajax({
        type: "POST",
        dataType: "json",
        url: href,  // check_out_storage_space_id
        data: {"check_out_storage_space_id": checkout_storage_space_id, "checkout_out_date": dateToMySqlFormat(check_out_date), "_token": this_csrf_token},
        success: function (response) {
            if (response.user_role_access == 'operation') {
                document.location = '/admin/bookings-and-availability'
            } else {
                document.location = '/admin/box-rooms/' + checkout_storage_space_id + '/edit'
            }
        },
        error: function (error) {
            popupErrorMessage(error.responseJSON.message)
        }
    });
} //bookingsAndAvailability.prototype.checkOutStorageSpace = function () {

// STORAGE SPACE CHECK OUT BLOCK END


// STORAGE SPACE CHECK IN BLOCK START

bookingsAndAvailability.prototype.openCheckInStorageSpaceDialog = function (id, number, select_client_id) {
    $('#additive_check_in_client_id').val("")
    $('#check_in_datepicker').val("")
    $('#check_in_job_ref_no').val("")
    $('#check_out_datepicker').val("")
    $("#div_additive_check_in_client_id").css("display", "block")
    $("#div_show_add_new_client_button").css("display", "block")
    $("#div_added_new_client_label").css("display", "none")

    $('#add_new_client_type').val("")

    $('#chosen_add_new_client_account_type').val("")
    $('#add_new_client_account_type').val("")

    $('#chosen_add_new_client_is_frank').val("")
    $('#add_new_client_is_frank').val("")

    $('#chosen_add_new_client_nationality_id').val("")
    $('#add_new_client_nationality_id').val("")

    $('#chosen_add_new_client_customer_type').val("")
    $('#add_new_client_customer_type').val("")
    $('#add_new_client_chosen_customer_type').val("")
    $('#add_new_client_box_client_id').val("")
    $('#add_new_client_full_name').val("")
    $('#add_new_client_chosen_is_frank').val("")
    $('#add_new_client_address').val("")
    $('#add_new_client_po_box').val("")
    $('#add_new_client_office_fax').val("")

    $("#div_checking_add_new_client_section").css("display", "none")
    $("#div_checking_step_1_section").css("display", "block")

    if (select_client_id) {
        $("#div_additive_check_in_client_id").css("display", "block")
    } else {
        $("#div_additive_check_in_client_id").css("display", "none")
    }

    $('#numeric_check_out_vat').val(0)
    $('#check_out_vat').val(0)

    $('#numeric_check_insurance_vat').val(0)
    $('#check_insurance_vat').val(0)

    $('#numeric_check_om_vat').val(0)
    $('#check_om_vat').val(0)

    $('#numeric_check_electricity_vat').val(0)
    $('#check_electricity_vat').val(0)

    $('#numeric_check_internet_vat').val(0)
    $('#check_internet_vat').val(0)

    $('#numeric_check_racks_vat').val(0)
    $('#check_racks_vat').val(0)

    $('#numeric_check_hot_desk_vat').val(0)
    $('#check_hot_desk_vat').val(0)

    $("#div_check_in_storage_space_modal").modal({
        "backdrop": "static",
        "keyboard": true,
        "show": true
    });

    bookingsAndAvailability.showClientAdditivePhonesListing('clear', '')
    bookingsAndAvailability.showClientAdditiveEmailsListing('clear', '')


    $('#check_in_storage_space_id').val(id)
    $('#span_check_in_number').html(number)
    this_current_check_in_step = 'check_in_step_1'
    $('#span_current_check_in_step').html(this_current_check_in_step)
    $("#button_check_in_storage_space").css("display", "block")
    $("#button_add_new_client").css("display", "none")

} // bookingsAndAvailability.prototype.openCheckInStorageSpaceDialog = function ( id, number ) {



bookingsAndAvailability.prototype.submitAddNewClient = function () {
    var add_new_client_full_name = $('#add_new_client_full_name').val()
    if (typeof add_new_client_full_name == "undefined" || add_new_client_full_name == "") {
        popupAlert("Fill client full name", 'danger')
        $('#add_new_client_full_name').focus()
        return;
    }

    var add_new_client_type = $('#add_new_client_type').val()
    console.log('add_new_client_type::')
    console.log(add_new_client_type)

    var add_new_client_account_type = $('#add_new_client_account_type').val()
    console.log('add_new_client_account_type::')
    console.log(add_new_client_account_type)

    if (typeof add_new_client_account_type == "undefined" || add_new_client_account_type == "") {
        popupAlert("Select Account Type !", 'danger')
        $('#chosen_add_new_client_account_type').focus()
        return;
    }

    var add_new_client_customer_type = $('#add_new_client_customer_type').val()
    // console.log('add_new_client_customer_type::')
    // console.log(add_new_client_customer_type)

    if (typeof add_new_client_customer_type == "undefined" || add_new_client_customer_type == "") {
        popupAlert("Select Customer Type !", 'danger')
        $('#chosen_add_new_client_customer_type').focus()
        return;
    }

    var add_new_client_nationality_id = $('#add_new_client_nationality_id').val()
    if (typeof add_new_client_nationality_id == "undefined" || add_new_client_nationality_id == "") {
        popupAlert("Select Nationality !", 'danger')
        $('#chosen_add_new_client_nationality_id').focus()
        return;
    }


    $("#div_checking_add_new_client_section").css("display", "none")
    $("#div_checking_step_1_section").css("display", "block")

    this_current_check_in_step = 'check_in_step_1'
    $('#span_current_check_in_step').html(this_current_check_in_step)
    $("#button_check_in_storage_space").css("display", "block")
    $("#button_add_new_client").css("display", "none")

    console.log('this_add_new_client_selected_existing::')
    console.log(this_add_new_client_selected_existing)

    if ( this_add_new_client_selected_existing ) {
        console.log('+++ this_add_new_client_selected_existing::')
        $("#div_additive_check_in_client_id").css("display", "block")
        $("#div_show_add_new_client_button").css("display", "none")
        $("#div_added_new_client_label").css("display", "none")
    } else {
        console.log('----- this_add_new_client_selected_existing::')
        $("#div_additive_check_in_client_id").css("display", "none")
        $("#div_show_add_new_client_button").css("display", "none")
        $("#div_added_new_client_label").css("display", "block")
        $("#div_added_new_client_full_name_label").html(add_new_client_full_name)
    }
} // bookingsAndAvailability.prototype.submitAddNewClient = function () {

bookingsAndAvailability.prototype.cancelAddNewClient = function () {
    console.log('?? cancelAddNewClient::')
    $("#div_check_in_storage_space_modal").modal('hide');
    var href = this_backend_home_url + "/admin/get-add-new-client-clear";
    $.ajax({
        type: "GET",
        dataType: "json",
        url: href,
        success: function (response) {
            console.log('++ cancelAddNewClient response::')
            console.log(response)
        },
        error: function (error) {
            popupErrorMessage(error.responseJSON.message)
        }
    });
} // bookingsAndAvailability.prototype.cancelAddNewClient = function () {

bookingsAndAvailability.prototype.checkInStorageSpace = function () {
    var add_new_client_full_name = $('#add_new_client_full_name').val()
    // console.log('add_new_client_full_name::')
    // console.log(add_new_client_full_name)


    var check_in_action_id = $('#check_in_action_id').val()
    if (check_in_action_id == '') {
        check_in_action_id = $('#additive_check_in_client_id').val()
    }
    // console.log('checkInStorageSpace check_in_action_id::')
    // console.log(check_in_action_id)

    var check_in_storage_space_id = $('#check_in_storage_space_id').val()
    var check_in_datepicker = $('#check_in_datepicker').val()
    var check_in_job_ref_no = $('#check_in_job_ref_no').val()
    var check_out_datepicker = $('#check_out_datepicker').val()
    var check_out_vat = jQuery.trim($('#check_out_vat').val())
    var check_insurance_vat = jQuery.trim($('#check_insurance_vat').val())
    var check_om_vat = jQuery.trim($('#check_om_vat').val())
    var check_electricity_vat = jQuery.trim($('#check_electricity_vat').val())
    var check_internet_vat = jQuery.trim($('#check_internet_vat').val())
    var check_racks_vat = jQuery.trim($('#check_racks_vat').val())
    var check_hot_desk_vat = jQuery.trim($('#check_hot_desk_vat').val())



    if (add_new_client_full_name == "") { // no  new client was added
        if (typeof check_in_action_id == "undefined" || check_in_action_id == "") {
            popupAlert("Select client !", 'danger')
            $('#additive_check_in_client_id').focus()
            return;
        }
    } //     if (add_new_client_full_name == "") { // no  new client was added


    if (typeof check_in_datepicker == "undefined" || check_in_datepicker == "") {
        popupAlert("Select check in date !", 'danger')
        $('#check_in_datepicker').focus()
        return;
    }

    if (typeof check_out_vat == "undefined" || check_out_vat == "" /*|| check_out_vat == 0*/) {
        popupAlert("Enter vat sum !", 'danger')
        $('#check_out_vat').focus()
        return;
    }

    var check_in_date = new Date(check_in_datepicker.replace(/,/gm, ''))
    var check_out_date = new Date(check_out_datepicker.replace(/,/gm, ''))

    if (check_in_date > check_out_date) {
        popupAlert("Check out can not be less of check in date !", 'danger')
        $('#check_out_datepicker').focus()
        return;
    }

    var href = this_backend_home_url + "/admin/check-in/make";
    $.ajax({
        type: "POST",
        dataType: "json",
        url: href,
        data: {
            "check_in_storage_space_id": check_in_storage_space_id,
            "check_in_action_id": check_in_action_id,
            "check_out_vat": check_out_vat,
            "check_insurance_vat": check_insurance_vat,
            "check_om_vat": check_om_vat,
            "check_electricity_vat": check_electricity_vat,
            "check_internet_vat": check_internet_vat,
            "check_racks_vat": check_racks_vat,
            "check_hot_desk_vat": check_hot_desk_vat,
            "check_in_date": dateToMySqlFormat(check_in_date),
            "check_out_date": dateToMySqlFormat(check_out_date),
            "check_in_job_ref_no": check_in_job_ref_no,

            "this_add_new_client_selected_existing": ( this_add_new_client_selected_existing ? 1 : 0 ),

            "add_new_client_type": $("#add_new_client_type").val(),
            "add_new_client_account_type": $("#add_new_client_account_type").val(),
            "add_new_client_customer_type": $("#add_new_client_customer_type").val(),
            "add_new_client_box_client_id": $("#add_new_client_box_client_id").val(),
            "add_new_client_full_name": $("#add_new_client_full_name").val(),
            "add_new_client_is_frank": $("#add_new_client_is_frank").val(),
            "add_new_client_nationality_id": $("#add_new_client_nationality_id").val(),
            "add_new_client_address": $("#add_new_client_address").val(),
            "add_new_client_po_box": $("#add_new_client_po_box").val(),
            "add_new_client_office_fax": $("#add_new_client_office_fax").val(),

            "_token": this_csrf_token
        },
        success: function (response) {
            if (response.user_role_access == 'operation') {
                document.location = '/admin/bookings-and-availability'
            } else {
                document.location = '/admin/box-rooms/' + check_in_storage_space_id + '/edit'
            }
        },
        error: function (error) {
            popupErrorMessage(error.responseJSON.message)
        }
    });
} // bookingsAndAvailability.prototype.checkInStorageSpace = function () {

// STORAGE SPACE CHECK IN BLOCK END


// STORAGE SPACE RESERVE BLOCK START
bookingsAndAvailability.prototype.openReserveStorageSpaceDialog = function (id, number, select_client_id) {
    if (select_client_id) {
        $("#div_additive_reserve_client_id").css("display", "block")
    } else {
        $("#div_additive_reserve_client_id").css("display", "none")
    }

    $('#reserve_out_vat').val(0)
    $("#div_reserve_storage_space_modal").modal({
        "backdrop": "static",
        "keyboard": true,
        "show": true
    });
    $('#reserve_storage_space_id').val(id)
    $('#span_reserve_number').html(number)



    ///
    $('#reserve_storage_space_id').val(id)
    $('#span_reserve_number').html(number)
    this_current_reserve_step = 'reserve_step_1'
    $('#span_current_reserve_step').html(this_current_reserve_step)     // remove
    $("#button_reserve_storage_space").css("display", "block")
    $("#button_add_new_client_reserve").css("display", "none")


} // bookingsAndAvailability.prototype.openReserveStorageSpaceDialog = function ( id, number ) {



bookingsAndAvailability.prototype.submitAddNewClientReserve = function () {
    var add_new_client_reserve_full_name = $('#add_new_client_reserve_full_name').val()
    if (typeof add_new_client_reserve_full_name == "undefined" || add_new_client_reserve_full_name == "") {
        popupAlert("Fill client full name", 'danger')
        $('#add_new_client_reserve_full_name').focus()
        return;
    }

    var add_new_client_reserve_type = $('#add_new_client_reserve_type').val()
    console.log('submitAddNewClientReserve add_new_client_reserve_type::')
    console.log(add_new_client_reserve_type)


    var add_new_client_reserve_account_type = $('#add_new_client_reserve_account_type').val()
    console.log('submitAddNewClientReserveadd_new_client_reserve_account_type::')
    console.log(add_new_client_reserve_account_type)

    if (typeof add_new_client_reserve_account_type == "undefined" || add_new_client_reserve_account_type == "") {
        popupAlert("Select Account Type !", 'danger')
        $('#chosen_add_new_client_reserve_account_type').focus()
        return;
    }

    var add_new_client_reserve_customer_type = $('#add_new_client_reserve_customer_type').val()
    console.log('add_new_client_reserve_customer_type::')
    console.log(add_new_client_reserve_customer_type)

    if (typeof add_new_client_reserve_customer_type == "undefined" || add_new_client_reserve_customer_type == "") {
        popupAlert("Select Customer Type !", 'danger')
        $('#chosen_add_new_client_reserve_customer_type').focus()
        return;
    }

    var add_new_client_reserve_nationality_id = $('#add_new_client_reserve_nationality_id').val()
    if (typeof add_new_client_reserve_nationality_id == "undefined" || add_new_client_reserve_nationality_id == "") {
        popupAlert("Select Nationality !", 'danger')
        $('#chosen_add_new_client_reserve_nationality_id').focus()
        return;
    }


    $("#div_reserve_add_new_client_section").css("display", "none")
    $("#div_reserve_step_1_section").css("display", "block")

    this_current_reserve_step = 'reserve_step_1'
    $('#span_current_reserve_step').html(this_current_reserve_step)
    $("#button_reserve_storage_space").css("display", "block")
    $("#button_add_new_client_reserve").css("display", "none")

    console.log('this_add_new_client_reserve_selected_existing::')
    console.log(this_add_new_client_reserve_selected_existing)


    if ( this_add_new_client_reserve_selected_existing ) {
        console.log('+++ this_add_new_client_reserve_selected_existing::')
        $("#div_additive_reserve_client_id").css("display", "block")
        $("#div_show_add_new_client_reserve_button").css("display", "none")
        $("#div_added_new_client_reserve_label").css("display", "none")
    } else {
        console.log('----- this_add_new_client_reserve_selected_existing::')
        $("#div_additive_reserve_client_id").css("display", "none")
        $("#div_show_add_new_client_reserve_button").css("display", "none")
        $("#div_added_new_client_reserve_label").css("display", "block")
        $("#div_added_new_client_reserve_full_name_label").html(add_new_client_reserve_full_name)
    }
} // bookingsAndAvailability.prototype.submitAddNewClientReserve = function () {


bookingsAndAvailability.prototype.reserveStorageSpace = function () {
    var reserve_storage_space_id = $('#reserve_storage_space_id').val()
    var reserve_action_id = $('#reserve_action_id').val()

    var reserve_datepicker = $('#reserve_datepicker').val()
    var reserve_out_datepicker = $('#reserve_out_datepicker').val()

    if (reserve_action_id == '') {
        reserve_action_id = $('#additive_reserve_client_id').val()
    }


    if (typeof reserve_action_id == "undefined" || reserve_action_id == "") {
        popupAlert("Select client !", 'danger')
        $('#additive_reserve_client_id').focus()
        return;
    }

    if (typeof reserve_datepicker == "undefined" || reserve_datepicker == "") {
        popupAlert("Select reserve date !", 'danger')
        $('#reserve_datepicker').focus()
        return;
    }
    if (typeof reserve_out_datepicker == "undefined" || reserve_out_datepicker == "") {
        popupAlert("Select reserve out date !", 'danger')
        $('#reserve_out_datepicker').focus()
        return;
    }

    var reserve_date = new Date(reserve_datepicker.replace(/,/gm, ''))
    var reserve_out_date = new Date(reserve_out_datepicker.replace(/,/gm, ''))

    if (reserve_date > reserve_out_date) {
        popupAlert("Reserve out can not be less of reserve date !", 'danger')
        $('#reserve_out_datepicker').focus()
        return;
    }

    var href = this_backend_home_url + "/admin/reserve/make";

    $.ajax({
        type: "POST",
        dataType: "json",
        url: href,
        data: {
            "reserve_storage_space_id": reserve_storage_space_id,
            "reserve_action_id": reserve_action_id,
            "reserve_date": dateToMySqlFormat(reserve_date),
            "reserve_out_date": dateToMySqlFormat(reserve_out_date),
            "_token": this_csrf_token
        },
        success: function (response) {
            if (response.user_role_access == 'operation') {
                document.location = '/admin/bookings-and-availability'
            } else {
                document.location = '/admin/box-rooms/' + reserve_storage_space_id + '/edit'
            }
        },
        error: function (error) {
            popupErrorMessage(error.responseJSON.message)
        }
    });
}

bookingsAndAvailability.prototype.openUnReserveStorageSpace = function (id, number) {
    var href = this_backend_home_url + "/admin/get-box-room-status-info-by-id/" + id;
    $.ajax({
        type: "get",
        dataType: "json",
        url: href,
        success: function (response) {

            if (typeof response.storageSpaceReserve == "undefined") {
                $('#unreserved_by_client_full_name').val("")
                $('#unreserve_vat').val("")
                $('#unreserve_date').val("")
                $('#unreserve_out_date').val("")
            } else {
                $('#unreserved_by_client_full_name').val(response.storageSpaceReserve.reserved_by_client_full_name)
                $('#unreserve_vat').val(response.storageSpaceReserve.vat)
                $('#unreserve_date').val(response.storageSpaceReserve.reserve_date)
                $('#unreserve_out_date').val(response.storageSpaceReserve.reserve_out_date)
            }
            $("#div_unreserve_storage_space_modal").modal({
                "backdrop": "static",
                "keyboard": true,
                "show": true
            });
            $('#unreserve_storage_space_id').val(id)
            $('#span_unreserve_number').html(number)
        },
        error: function (error) {
            popupErrorMessage(error.responseJSON.message)
        }
    });

} // bookingsAndAvailability.prototype.openUnReserveStorageSpace = function ( id, number ) {


bookingsAndAvailability.prototype.unReserveStorageSpace = function () {
    var unreserve_storage_space_id = $('#unreserve_storage_space_id').val()
    var href = this_backend_home_url + "/admin/unreserve/make";
    $.ajax({
        type: "POST",
        dataType: "json",
        url: href,
        data: {"unreserve_storage_space_id": unreserve_storage_space_id, "_token": this_csrf_token},
        success: function (response) {
            if (response.user_role_access == 'operation') {
                document.location = '/admin/bookings-and-availability'
            } else {
                document.location = '/admin/box-rooms/' + unreserve_storage_space_id + '/edit'
            }
        },
        error: function (error) {
            popupErrorMessage(error.responseJSON.message)
        }
    });
} //bookingsAndAvailability.prototype.unReserveStorageSpace = function () {


bookingsAndAvailability.prototype.showAddNewClientReserveForm = function () {
    console.log('showAddNewClientReserveForm::')

    $("#div_reserve_add_new_client_section").css("display", "block")
    $("#div_reserve_step_1_section").css("display", "none")

    this_current_reserve_step = 'add_new_client_reserve'
    $('#span_current_reserve_step').html(this_current_reserve_step)  // REMOVE ???
    $("#button_reserve_storage_space").css("display", "none")
    $("#button_add_new_client_reserve").css("display", "block")
    $('#additive_reserve_client_id').val("")
    $('#add_new_client_reserve_full_name').focus()
    this_id= -1

} // bookingsAndAvailability.prototype.showAddNewClientReserveForm = function () {

bookingsAndAvailability.prototype.addNewClientReserveFullNameOnChange = function () {
    var add_new_client_reserve_full_name = jQuery.trim($("#add_new_client_reserve_full_name").val())
    console.log('addNewClientReserveFullNameOnChange::')
    console.log(add_new_client_reserve_full_name)
    var href = this_backend_home_url + "/admin/get-client-full-name/" + add_new_client_reserve_full_name+"/1/reserve_";
    $.ajax({
        type: "GET",
        dataType: "json",
        url: href,
        success: function (response) {
            console.log('addNewClientReserveFullNameOnChange response::')
            console.log(response)
            console.log(typeof response.client)
            console.log(response.client)
            if (response.client == null) {
                popupErrorMessage("Client not found !")
                $('#add_new_client_reserve_type').val("")
                $('#chosen_add_new_client_reserve_account_type').val("")
                $('#add_new_client_reserve_account_type').val("")
                $('#chosen_add_new_client_reserve_customer_type').val("")
                $('#add_new_client_reserve_customer_type').val("")
                $('#add_new_client_reserve_box_client_id').val("")
                $('#chosen_add_new_client_reserve_is_frank').val("")
                $('#add_new_client_reserve_is_frank').val("")

                $('#chosen_add_new_client_reserve_nationality_id').val("")
                $('#add_new_client_reserve_nationality_id').val("")

                $('#add_new_client_reserve_address').val("")
                $('#add_new_client_reserve_po_box').val("")
                $('#add_new_client_reserve_office_fax').val("")
                this_add_new_client_reserve_selected_existing= false
                $('#additive_reserve_client_id').val("")
                bookingsAndAvailability.showClientAdditivePhonesListing('clear', 'reserve_')
                bookingsAndAvailability.showClientAdditiveEmailsListing('clear', 'reserve_')
                return
            }

            $('#add_new_client_reserve_type').val(response.client.type)
            $('#chosen_add_new_client_reserve_account_type').val(response.client.account_type)
            $('#add_new_client_reserve_account_type').val(response.client.account_type)

            $('#chosen_add_new_client_reserve_customer_type').val(response.client.customer_type)
            $('#add_new_client_reserve_customer_type').val(response.client.customer_type)

            $('#add_new_client_reserve_box_client_id').val(response.client.box_client_id)

            $('#chosen_add_new_client_reserve_is_frank').val(response.client.is_frank)
            $('#add_new_client_reserve_is_frank').val(response.client.is_frank)

            $('#chosen_add_new_client_reserve_nationality_id').val(response.client.nationality_id)
            $('#add_new_client_reserve_nationality_id').val(response.client.nationality_id)

            $('#add_new_client_reserve_address').val(response.client.address)
            $('#add_new_client_reserve_po_box').val(response.client.po_box)
            $('#add_new_client_reserve_office_fax').val(response.client.office_fax)

            this_id= response.client.id
            bookingsAndAvailability.showClientAdditivePhonesListing('', 'reserve_')
            bookingsAndAvailability.showClientAdditiveEmailsListing('', 'reserve_')

            this_add_new_client_reserve_selected_existing= true

            var is_client_id_in_options= false
            $("#additive_reserve_client_id option").each(function()
            {
                if ( $(this).val() == response.client.id ) {
                    is_client_id_in_options= true
                }
            });
            if (!is_client_id_in_options) {
                addDDLBItem("additive_reserve_client_id", response.client.id, add_new_client_reserve_full_name)
            }
            $('#additive_reserve_client_id').val(response.client.id)

            popupAlert("Client was found and fields were filled !", 'success')
        },
        error: function (error) {
            popupErrorMessage("Client Not Found !")
            this_add_new_client_reserve_selected_existing= false
            this_id= -1
            bookingsAndAvailability.showClientAdditivePhonesListing('clear', 'reserve_')
            bookingsAndAvailability.showClientAdditiveEmailsListing('clear', 'reserve_')
            // popupErrorMessage(error.responseJSON.message)
        }
    });

} // bookingsAndAvailability.prototype.addNewClientReserveFullNameOnChange = function ( ) {


// STORAGE SPACE RESERVE BLOCK END


// SET STORAGE COLORS BLOCK START

bookingsAndAvailability.prototype.saveBoxRoomColorId = function (storage_space_id) {
    var color_id = $("#box_room_color_id_" + storage_space_id).val()

    var href = this_backend_home_url + "/admin/save-room-box-color-id";
    $.ajax({
        type: "POST",
        dataType: "json",
        url: href,
        data: {
            "storage_space_id": storage_space_id,
            "color_id": color_id,
            "_token": this_csrf_token
        },
        success: function (response) {
            var l = this_colorValuesArray.length

            for (var i = 0; i < l; i++) {
                if (this_colorValuesArray[i][1].id == color_id) {
                    $("#span_id_box_rooms_color_" + storage_space_id).css("background-color", this_colorValuesArray[i][1].color)
                    $("#span_box_rooms_color_" + storage_space_id).css("background-color", this_colorValuesArray[i][1].color)
                    $("#span_box_rooms_color_" + storage_space_id).prop('title', response.storage_space_selected_status_label + ' : ' + response.selected_color_status_label)
                    $("#span_storage_space_status_label_" + storage_space_id).html('')
                    $("#span_storage_space_status_label_" + storage_space_id).html(response.selected_color_status_label)
                    // $("#span_storage_space_status_label_" + storage_space_id).html(response.storage_space_selected_status_label)
                    $(".storage_space_color_block_" + storage_space_id).toggle();
                }
            }
            popupAlert("Color of Box room was successfully changed !", 'success')
        },
        error: function (error) {
            popupErrorMessage(error.responseJSON.message)
        }
    });
} //bookingsAndAvailability.prototype.saveBoxRoomColorId = function () {


bookingsAndAvailability.prototype.showStorageSpaceColorBlock = function (storage_space_id) {
    $(".storage_space_color_block_" + storage_space_id).toggle();
}

bookingsAndAvailability.prototype.onchangeSubstatusColor = function () {
    var color_id = $("#color_id").val()
    var l = this_colorValuesArray.length
    for (var i = 0; i < l; i++) {
        if (this_colorValuesArray[i][1].id == color_id) {
            $("#label_color_id").css("background-color", this_colorValuesArray[i][1].color)
        }
    }
}


bookingsAndAvailability.prototype.showAddNewClientForm = function () {
    $("#div_checking_add_new_client_section").css("display", "block")
    $("#div_checking_step_1_section").css("display", "none")
    this_current_check_in_step = 'add_new_client'
    $('#span_current_check_in_step').html(this_current_check_in_step)
    $("#button_check_in_storage_space").css("display", "none")
    $("#button_add_new_client").css("display", "block")
    $('#additive_check_in_client_id').val("")
    $('#add_new_client_full_name').focus()
    this_id= -1
    bookingsAndAvailability.showClientAdditivePhonesListing('clear', 'reserve_')
    bookingsAndAvailability.showClientAdditiveEmailsListing('clear', 'reserve_')

} // bookingsAndAvailability.prototype.showAddNewClientForm = function () {

// SET STORAGE COLORS BLOCK END


// CLIENT ADDITIVE PHONES BLOCK BEGIN
/*     Route::get('/get-add-new-client-client-additive-phones-listing/{client_id}', 'Admin\CheckController@get_add_new_client_client_additive_phones_listing')->name('get-add-new-client-client-additive-phones-listing');
 Route::delete('/add-new-client-client-additive-phone-destroy', 'Admin\CheckController@add_new_client_client_additive_phone_destroy');
 Route::post('/add-new-client-add-client-additive-phone', 'Admin\CheckController@add_new_client_add_client_additive_phone');
*/
bookingsAndAvailability.prototype.showClientAdditivePhonesListing = function (action, data_type) {
    // alert( 'get-add-new-client-client-additive-phones-listing action::' + action +"  data_type::"+data_type )
    if (typeof this_id == "undefined" || jQuery.trim(this_id) == "") {
        this_id = -1;
    }
    if (typeof action != "undefined" && jQuery.trim(action) != "") {
        this_id = action;
    }

    console.log('showClientAdditivePhonesListing this_id::')
    console.log(this_id)

    console.log('showClientAdditivePhonesListing data_type::')
    console.log(data_type)

    var href = this_backend_home_url + "/admin/get-add-new-client-client-additive-phones-listing/" + this_id+"/"+data_type;
    $.ajax({
        type: "GET",
        dataType: "json",
        url: href,
        success: function (response) {
            console.log('++ showClientAdditivePhonesListing response::')
            console.log(response)
            $("#div_add_new_client_"+data_type+"additive_phones").html(response.html);
            this_id = -1;
        },
        error: function (error) {
            popupErrorMessage(error.responseJSON.message)
        }
    });

} // showClientAdditivePhonesListing

bookingsAndAvailability.prototype.deleteClientAdditivePhone = function (id, client_id, data_type, phone) {
    confirmMsg('Do you want to delete "' + phone + '" phone ?', function () {
            var href = this_backend_home_url + "/admin/add-new-client-client-additive-phone-destroy";
            $.ajax({
                type: "DELETE",
                dataType: "json",
                url: href,
                data: {"id": id, "_token": this_csrf_token},
                success: function (response) {
                    bookingsAndAvailability.showClientAdditivePhonesListing('', data_type)
                },
                error: function (error) {
                    popupErrorMessage(error.responseJSON.message)
                }
            });

        }
    );

} // bookingsAndAvailability.prototype.deleteClientAdditivePhone = function ( id, client_id, name ) {

bookingsAndAvailability.prototype.addClientAdditivePhone = function (client_id, data_type) {
    var add_new_phone = jQuery.trim($("#"+data_type+"add_new_phone").val())
    var add_new_phone_type = jQuery.trim($("#"+data_type+"add_new_phone_type").val())

    if (add_new_phone == "") {
        popupAlert("Enter phone !", 'danger')
        $("#"+data_type+"add_new_phone").focus()
        return;
    }

    if (add_new_phone_type == "") {
        popupAlert("Select phone type !", 'danger')
        $("#"+data_type+"add_new_phone_type").focus()
        return;
    }

    var href = this_backend_home_url + "/admin/add-new-client-add-client-additive-phone";
    $.ajax({
        type: "POST",
        dataType: "json",
        url: href,
        data: {"client_id": client_id,
            "add_new_phone_type": add_new_phone_type,
            "add_new_phone": add_new_phone,
            "data_type": data_type,
            "_token": this_csrf_token
        },
        success: function (response) {
            console.log('add-new-client-add-client-additive-phone response::')
            console.log(response)

            bookingsAndAvailability.showClientAdditivePhonesListing('', data_type)
        },
        error: function (error) {
            popupErrorMessage(error.responseJSON.message)
        }
    });

} // bookingsAndAvailability.prototype.addClientAdditivePhone = function ( client_id ) {

// CLIENT ADDITIVE PHONES BLOCK END


// CLIENT ADDITIVE EMAILS BLOCK BEGIN
bookingsAndAvailability.prototype.showClientAdditiveEmailsListing = function (action, data_type) {
    // alert( 'action::' + action)
    if (typeof this_id == "undefined" || jQuery.trim(this_id) == "") {
        this_id = -1;
    }
    if (typeof action != "undefined" && jQuery.trim(action) != "") {
        this_id = action;
    }

    console.log('showClientAdditiveEmailsListing this_id::')
    console.log(this_id)

    console.log('showClientAdditiveEmailsListing data_type::')
    console.log(data_type)

    var href = this_backend_home_url + "/admin/get-add-new-client-client-additive-emails-listing/" + this_id + "/" + data_type;
    $.ajax({
        type: "GET",
        dataType: "json",
        url: href,
        success: function (response) {
            console.log('showClientAdditiveEmailsListing response::')
            console.log(response)

            $("#div_add_new_client_"+data_type+"client_additive_emails").html(response.html);
            this_id = -1;
        },
        error: function (error) {
            popupErrorMessage(error.responseJSON.message)
        }
    });

} // showClientAdditiveEmailsListing

bookingsAndAvailability.prototype.deleteClientAdditiveEmail = function (id, client_id, data_type, email) {
    console.log('data_type::')
    console.log(data_type)

    confirmMsg('Do you want to delete "' + email + '" email ?', function () {
            var href = this_backend_home_url + "/admin/add-new-client-client-additive-email-destroy";
            $.ajax({
                type: "DELETE",
                dataType: "json",
                url: href,
                data: {"id": id, "data_type": data_type, "_token": this_csrf_token},
                success: function (response) {
                    bookingsAndAvailability.showClientAdditiveEmailsListing('', data_type)
                },
                error: function (error) {
                    popupErrorMessage(error.responseJSON.message)
                }
            });

        }
    );

} // bookingsAndAvailability.prototype.deleteClientAdditiveEmail = function ( id, client_id, name ) {

bookingsAndAvailability.prototype.addClientAdditiveEmail = function (client_id, data_type) {
    // alert( 'addClientAdditiveEmail data_type::'+data_type )
    var add_new_email = jQuery.trim($("#"+data_type+"add_new_email").val())

    if (add_new_email == "") {
        popupAlert("Enter email !", 'danger')
        $("#"+data_type+"add_new_email").focus()
        return;
    }

    var href = this_backend_home_url + "/admin/add-new-client-add-client-additive-email";
    console.log('client_id::')
    console.log(client_id)

    console.log('addClientAdditiveEmail add_new_email::')
    console.log(add_new_email)

    console.log('addClientAdditiveEmail this_csrf_token::')
    console.log(this_csrf_token)

    console.log('addClientAdditiveEmail data_type::')
    console.log(data_type)

    $.ajax({
        type: "POST",
        dataType: "json",
        url: href,
        data: {"client_id": client_id, "add_new_email": add_new_email, "_token": this_csrf_token, data_type : data_type},
        success: function (response) {
            console.log('addClientAdditiveEmai response::')
            console.log(response)

            bookingsAndAvailability.showClientAdditiveEmailsListing('', data_type)
        },
        error: function (error) {
            popupErrorMessage(error.responseJSON.message)
        }
    });

} // bookingsAndAvailability.prototype.addClientAdditiveEmail = function ( client_id ) {

// CLIENT ADDITIVE EMAILS BLOCK END


bookingsAndAvailability.prototype.addNewClientFullNameOnChange = function () {
    var add_new_client_full_name = jQuery.trim($("#add_new_client_full_name").val())
    let data_type= ''
    alert( 'data_type::'+data_type )
    console.log('addNewClientFullNameOnChange::')
    console.log(add_new_client_full_name)
    var href = this_backend_home_url + "/admin/get-client-full-name/" + add_new_client_full_name+"/1";
    $.ajax({
        type: "GET",
        dataType: "json",
        url: href,
        success: function (response) {
            console.log('response::')
            console.log(response)
            console.log(typeof response.client)
            console.log(response.client)
            if (response.client == null) {
                popupErrorMessage("Client not found !")
                $('#add_new_client_type').val("")
                $('#chosen_add_new_client_account_type').val("")
                $('#add_new_client_account_type').val("")
                $('#chosen_add_new_client_customer_type').val("")
                $('#add_new_client_customer_type').val("")
                $('#add_new_client_box_client_id').val("")
                $('#chosen_add_new_client_is_frank').val("")
                $('#add_new_client_is_frank').val("")

                $('#chosen_add_new_client_nationality_id').val("")
                $('#add_new_client_nationality_id').val("")

                $('#add_new_client_address').val("")
                $('#add_new_client_po_box').val("")
                $('#add_new_client_office_fax').val("")
                this_add_new_client_selected_existing= false
                $('#additive_check_in_client_id').val("")
                bookingsAndAvailability.showClientAdditivePhonesListing('clear', data_type)
                bookingsAndAvailability.showClientAdditiveEmailsListing('clear', data_type)

                return
            }

            $('#add_new_client_type').val(response.client.type)
            $('#chosen_add_new_client_account_type').val(response.client.account_type)
            $('#add_new_client_account_type').val(response.client.account_type)

            $('#chosen_add_new_client_customer_type').val(response.client.customer_type)
            $('#add_new_client_customer_type').val(response.client.customer_type)

            $('#add_new_client_box_client_id').val(response.client.box_client_id)

            $('#chosen_add_new_client_is_frank').val(response.client.is_frank)
            $('#add_new_client_is_frank').val(response.client.is_frank)

            $('#chosen_add_new_client_nationality_id').val(response.client.nationality_id)
            $('#add_new_client_nationality_id').val(response.client.nationality_id)

            $('#add_new_client_address').val(response.client.address)
            $('#add_new_client_po_box').val(response.client.po_box)
            $('#add_new_client_office_fax').val(response.client.office_fax)

            this_id= response.client.id
            bookingsAndAvailability.showClientAdditivePhonesListing('', '')
            bookingsAndAvailability.showClientAdditiveEmailsListing('', '')
            this_add_new_client_selected_existing= true

            var is_client_id_in_options= false
            $("#additive_check_in_client_id option").each(function()
            {
                if ( $(this).val() == response.client.id ) {
                    is_client_id_in_options= true
                }
            });
            if (!is_client_id_in_options) {
                addDDLBItem("additive_check_in_client_id", response.client.id, add_new_client_full_name)
            }
            $('#additive_check_in_client_id').val(response.client.id)

            popupAlert("Client was found and fields were filled !", 'success')
        },
        error: function (error) {
            popupErrorMessage("Client Not Found !")
            this_add_new_client_selected_existing= false
            this_id= -1
            bookingsAndAvailability.showClientAdditivePhonesListing('clear', '')
            bookingsAndAvailability.showClientAdditiveEmailsListing('clear', '')
            // popupErrorMessage(error.responseJSON.message)
        }
    });

} // bookingsAndAvailability.prototype.addNewClientFullNameOnChange = function ( ) {

