
 // import faker from 'faker'
 // import 'cypress-file-upload'


function  getFilenameOfPath(Filename) {
    var valuesArray = Filename.split('/')
    console.log('valuesArray::')
    console.log(valuesArray)
    console.log(typeof valuesArray)

    if (typeof valuesArray[valuesArray.length-1] != "undefined") {
        let res = valuesArray[valuesArray.length-1]
        console.log('res++::')
        console.log(res)
        return res
    }
    return false
}



describe('Admin category fileupload functionality', () => {
    it('category fileupload', () => {
        // cy.visit('/')

        cy.visit('/login')

        const test_email = 'admin@site.com'
        const test_password = '111111'

        cy.get('#email')
            .clear()
            .type(test_email)
            .should('have.value', test_email)


        cy.get('#password')
            .clear()
            .type(test_password)
            .should('have.value', test_password)


        cy.get('button')
            .contains('Submit')
            .click()

        // cy.wait('.admin_access_rights')
        cy.wait(500)
        // cy.wait('#nav_admin')

        cy.get('article').should('have.class', 'page_content_container')

        cy.get('div.top_nav_menu')
            .find('.admin_access_rights')
            .click()


        cy.visit('/admin/categories/edit/15')
        cy.url()
            .should('include', '/admin/categories/edit/15') // newly inserted row must be redirected to the editor in edit mode with ID in url
            .then((response) => {
                console.log('==response::')
                console.log(response)  // like /admin/categories/edit/52
                // alert( '-1::' )
                const uploaded_file_name_path = '/sample_images/sample.png'
                // const fileName = 'http://local-ctasks-api.com/storage/categories/-category-70/1.png'

                cy.wait(500)

                cy.get('div.page_content')
                    .get('ul.nav-tabs')
                    .last()
                    .find('.tabs__link')
                    .last()
                    .click()

                cy.contains('Image Preview/Upload')

                cy.fixture(uploaded_file_name_path).then(fileContent => {
                    console.log('++Inside Tests cy::')
                    console.log(cy)

                    cy.get('#file').upload({ fileContent, uploaded_file_name_path, mimeType: 'image/png' })
                    console.log('!!!AFTER Inside Tests cy::')

                    cy.get('button')
                        .contains('Upload')
                        .click()


                    let filename= this.getFilenameOfPath(uploaded_file_name_path)
                    console.log('!!!!!!++filename::')
                    console.log(filename)

                    cy.get('div.uploded_file_info')
                        .contains(filename)
                        // .click()

                })
            })

    })

})



