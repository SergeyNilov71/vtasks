import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import {bus} from '../main'

import {settingCredentialsConfig} from '@/app.settings.js'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {// store data
        status: '',
        token: localStorage.getItem('token') || '',
        user: null,
        avatar_url: '',
        taskAssignedToUsers: [],
        userSkills: [],
        userGroups: [],
        is_admin: '',
        is_manager: ''
    }, // state: { // store data

    getters: {

        token(state) {
            if (state.token.length > 0) {
                return state.token
            }
            let token = localStorage.getItem('token')
            if (token != null && typeof token === 'string') {
                if (token.length > 0) {
                    return token
                }
            }
            return ''
        }, // token(state) {

        isLoggedIn: state => !!state.token,
        // authStatus: state => state.status,

        user(state) {
            if (state.user != null && typeof state.user.email === 'string') {
                return state.user
            }
            let user = localStorage.getItem("user")
            if (user != null && user != '') {
                if (isValidJsonString(user)) {
                    let localStorageUser = JSON.parse(user)
                    if (typeof localStorageUser === 'object') {
                        return localStorageUser
                    }
                }
            }
            return null
        }, // user(state) {

        avatar_url(state) {
            if (state.avatar_url.length > 0) {
                return state.avatar_url
            }
            let avatar_url = localStorage.getItem('avatar_url')
            if (avatar_url != null && typeof avatar_url === 'string') {
                if (avatar_url.length > 0) {
                    return avatar_url
                }
            }
            return ''
        }, // avatar_url(state) {

        taskAssignedToUsers(state) {
            if (state.taskAssignedToUsers.length > 0) {
                return state.taskAssignedToUsers
            }
            let taskAssignedToUsers = localStorage.getItem('taskAssignedToUsers')
            if (taskAssignedToUsers != null && isValidJsonString(taskAssignedToUsers)) {
                let localStorageTaskAssignedToUsers = JSON.parse(taskAssignedToUsers)
                if (localStorageTaskAssignedToUsers.length > 0) {
                    return localStorageTaskAssignedToUsers
                }
            }
            return []
        }, // taskAssignedToUsers(state) {

        userSkills(state) {
            if (state.userSkills.length > 0) {
                return state.userSkills
            }

            let userSkills = localStorage.getItem('userSkills')
            if (userSkills != null && isValidJsonString(userSkills) ) {
                let localStorageUserSkills = JSON.parse(userSkills)
                if (localStorageUserSkills.length > 0) {
                    return localStorageUserSkills
                }
            }
            return []
        }, // userSkills(state) {


        userGroups(state) {
            if (state.userGroups.length > 0) {
                return state.userGroups
            }
            let userGroups = localStorage.getItem('userGroups')
            if (userGroups != null && isValidJsonString(userGroups) ) {
                let localStorageUserGroups = JSON.parse(userGroups)
                if (localStorageUserGroups.length > 0) {
                    return localStorageUserGroups
                }
            }
            return []
        }, // userGroups(state) {

        is_manager(state) {
            if (state.is_manager) {
                return state.is_manager
            }
            let is_manager = localStorage.getItem('is_manager')
            if (is_manager != null && typeof is_manager === 'string') {
                if (is_manager) {
                    return true
                }
            }
            return ''
        }, // is_manager(state) {


        is_admin(state) {
            if (state.is_admin) {
                return state.is_admin
            }
            let is_admin = localStorage.getItem('is_admin')
            if (is_admin != null && typeof is_admin === 'string') {
                if (is_admin) {
                    return true
                }
            }
            return ''
        }, // is_admin(state) {


    }, // getters : {

    mutations: { // methods to make changes to vuex store
        auth_request(state) {
            state.status = 'loading'
        },

        auth_success(state, data) {
            state.status = 'success'
            state.token = data.token
            localStorage.setItem("token", data.token)

            if (typeof data.avatar_url != 'undefined') {
                state.avatar_url = data.avatar_url
                localStorage.setItem("avatar_url", data.avatar_url)
            }

            state.user = data.user
            localStorage.setItem("user", JSON.stringify(data.user))
        },

        auth_error(state) {
            state.status = 'error'
        },


        logout(state) {
            state.status = ''

            state.token = ''
            localStorage.setItem('token', '')

            state.avatar_url = ''
            localStorage.setItem('avatar_url', '')

            state.user = null
            localStorage.setItem('user', {})

            state.user = null
            localStorage.setItem('user', {})

            state.taskAssignedToUsers = []
            localStorage.setItem("taskAssignedToUsers", [])

            state.userSkills = []
            localStorage.setItem("userSkills", [])

            state.userGroups = []
            localStorage.setItem("userGroups", [])

            state.is_manager = false
            localStorage.setItem("is_manager", '')

            state.is_admin = false
            localStorage.setItem("is_admin", '')
            state.isLoggedIn = false
            // state.authStatus = ''

        },


        refreshTaskAssignedToUsers(state, payload) {
            state.taskAssignedToUsers = payload
            localStorage.setItem("taskAssignedToUsers", JSON.stringify(payload))
        },

        refreshUserSkills(state, payload) {
            state.userSkills = payload
            localStorage.setItem("userSkills", JSON.stringify(payload))
        },

        refreshUserGroups(state, payload) {
            state.userGroups = payload
            localStorage.setItem("userGroups", JSON.stringify(payload))
            state.userGroups.map((nextUserGroup) => {
                if (nextUserGroup.is_manager) {
                    state.is_manager = true
                    localStorage.setItem("is_manager", '1')
                    return
                }
            })

            state.userGroups.map((nextUserGroup) => {
                if (nextUserGroup.is_admin) {
                    state.is_admin = true
                    localStorage.setItem("is_admin", '1')
                    return
                }
            })


        },

    }, // mutations: { // methods to make changes to vuex store

    actions: { // async methods
        login({commit}, userCredentials) { // Login action
            return new Promise((resolve, reject) => {
                commit('auth_request')
                let apiUrl = process.env.VUE_APP_API_URL
                console.log('+login userCredentials::')
                console.log(userCredentials)

                console.log('+login settingCredentialsConfig::')
                console.log(settingCredentialsConfig)

                console.log('+login apiUrl::')
                console.log(apiUrl)

                // alert( 'apiUrl::'+apiUrl )
                axios.post(apiUrl + '/login', userCredentials, settingCredentialsConfig)
                    .then((response) => {
                        if (typeof response.data.access_token === 'undefined' || typeof response.data.user === 'undefined') {
                            commit('auth_error') // call auth_error mutation to make changes to vuex store
                            bus.$emit('authLoggedError')
                            return
                        }

                        const token = response.data.access_token
                        const user = response.data.user
                        let avatar_url= ''
                        if ( typeof response.data.filenameData != 'undefined')
                        {
                            avatar_url = response.data.filenameData.avatar_url
                        }
                        axios.defaults.headers.common['Authorization'] = token
                        commit('auth_success', {token: token, user: user, avatar_url : avatar_url }) // call auth_success mutation to make changes to vuex store
                        bus.$emit('authLoggedSuccess', user)
                        resolve(response)
                    })
                    .catch((error) => {
                        commit('auth_error') // call auth_error mutation to make changes to vuex store
                        localStorage.removeItem('token')
                        bus.$emit('authLoggedError')
                        reject(error)
                    })
            })
        }, // login ({ commit }, user) { // Login action

        register({commit}, userToRegister) {
            return new Promise((resolve, reject) => {
                commit('auth_request')
                let apiUrl = process.env.VUE_APP_API_URL

                let app_url= document.location.protocol+'//'+document.location.host
                userToRegister.app_url= app_url
                axios.post(apiUrl + '/register', userToRegister, settingCredentialsConfig)
                    .then((response) => {
                        // console.log('authRegisterSuccess response::')
                        // console.log(response)
                        //
                        // console.log('authRegisterSuccess response.data::')
                        // console.log(response.data)
                        //
                        // console.log('authRegisterSuccess response.data.success::')
                        // console.log(response.data.success)
                        // console.log('authRegisterSuccess response.data.success.user::')
                        // console.log(response.data.success.user)
                        //
                        bus.$emit('authRegisterSuccess', response.data.success.user)
                        resolve(response)
                    })
                    .catch((error) => {
                        console.error(error)
                        if (typeof error.response.data.errors != 'undefined') {
                            bus.$emit('authRegisterError', error.response.data.errors)
                        }
                        reject(error)
                    })

            })
        }, // register ({ commit }, userToRegister) {

        logout({commit}) {
            return new Promise((resolve/*, reject*/) => {
                commit('logout')
                localStorage.removeItem('token')
                delete axios.defaults.headers.common['Authorization']
                resolve()
            })
        }, // logout ({ commit }) {

        ///////////// TASK ASSIGNED TO USERS ASYNC METHODS (ACTIONS) BLOCK BEGIN ///////////////
        retrieveTaskAssignedToUsers(context) {
            let apiUrl = process.env.VUE_APP_API_URL
            settingCredentialsConfig.headers.Authorization = 'Bearer ' + this.getters.token
            axios.get(apiUrl + '/personal/task_assigned_to_users', settingCredentialsConfig)
                .then((response) => {
                    context.commit('refreshTaskAssignedToUsers', response.data.taskAssignedToUsers)
                })
                .catch(error => {
                    console.error(error)
                })
        }, // retrieveTaskAssignedToUsers(context) {

        taskAssignedToUserStore(context, taskAssignedToUser ) {
            let apiUrl = process.env.VUE_APP_API_URL
            settingCredentialsConfig.headers.Authorization = 'Bearer ' + this.getters.token
            bus.$emit( 'beforetaskAssignedToUserStore', taskAssignedToUser )
            axios.post(apiUrl + '/personal/task_assigned_to_user_store', taskAssignedToUser, settingCredentialsConfig ).then((response) => {
                let taskAssignedToUsers = this.getters.taskAssignedToUsers
                taskAssignedToUsers.push(taskAssignedToUser)
                bus.$emit( 'onTaskAssignedToUserStoreSuccess', response )
            }).catch((error) => {
                console.error(error)
                bus.$emit('onTaskAssignedToUserStoreFailure', error)
            })

        }, // taskAssignedToUserStore(context ) {

        ///////////// TASK ASSIGNED TO USERS ASYNC METHODS (ACTIONS) BLOCK END ///////////////


        ///////////// USER SKILLS ASYNC METHODS (ACTIONS) BLOCK BEGIN ///////////////
        retrieveUserSkills(context) {
            let apiUrl = process.env.VUE_APP_API_URL
            settingCredentialsConfig.headers.Authorization = 'Bearer ' + this.getters.token
            axios.get(apiUrl + '/personal/user_skills', settingCredentialsConfig)
                .then((response) => {
                    context.commit('refreshUserSkills', response.data.userSkills)
                })
                .catch(error => {
                    console.error(error)
                })
        }, // retrieveUserSkills(context) {
        ///////////// USER SKILLS ASYNC METHODS (ACTIONS) BLOCK END ///////////////


        ///////////// USER GROUPS ASYNC METHODS (ACTIONS) BLOCK BEGIN ///////////////
        retrieveUserGroups(context) {
            let apiUrl = process.env.VUE_APP_API_URL
            settingCredentialsConfig.headers.Authorization = 'Bearer ' + this.getters.token
            axios.get(apiUrl + '/personal/user_groups', settingCredentialsConfig)
                .then((response) => {
                    context.commit('refreshUserGroups', response.data.userGroups)
                })
                .catch(error => {
                    console.error(error)
                })
        }, // retrieveUserGroups(context) {
        ///////////// USER GROUPS ASYNC METHODS (ACTIONS) BLOCK END ///////////////

    }, // actions: // async methods

})

function isValidJsonString(str) {
    try {
        JSON.parse(str)
    } catch (e) {
        return false
    }
    return true
}
